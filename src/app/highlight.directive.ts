import {Directive, ElementRef, HostListener, Input} from '@angular/core';

@Directive({selector: '[appHighlight]'})
export class HighlightDirective {
  @Input('appHighlight') highlightColor: string;
  @Input() defaultColor: string;
  @Input() fontColor: string;

  constructor(private el: ElementRef) {
    console.log('directive init');
  }

  private changeFontColor(color: string) {
    this.el.nativeElement.style.color = this.fontColor;
  }

  private highlight(color: string) {
    this.el.nativeElement.style.backgroundColor = color;
  }

  // 鼠标悬浮而上时改变背景颜色
  @HostListener('mouseenter') onMouseEnter() {
    this.highlight(this.highlightColor || this.defaultColor || 'red');
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.highlight(null);
  }
}
