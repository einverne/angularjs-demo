import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  // 这里是整个 Angular 项目的根目录
  // PropertyName: PropertyType = Value
  title = '这是一个 AngularJS-demo 演示';
  isLogin = true;
  username = 'EV';
  navs = ['nav1', 'nav2'];
  links = [
    {
      'name': 'Google',
      'link': 'https://www.google.com'
    },
    {
      'name': 'YouTube',
      'link': 'https://www.youtube.com'
    }
  ];

  backgroudColor = 'green';
}
